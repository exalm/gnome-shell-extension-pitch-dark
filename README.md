Pitch-Dark
==========

A simple fork of https://github.com/pgaubatz/gnome-shell-extension-pitch-dark that only darkens certain windows, like GIMP 2.10 or Blender.